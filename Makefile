CODE = app tests
VENV ?= .venv

TEST = pytest --verbosity=2 --showlocals --log-level=DEBUG

help: ## Show help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: test
test: ## Run tests
	$(TEST)

test-cov: ## Run tests with coverage report 
	$(TEST) --cov --cov-report html

test-failed: # Run last failed tests
	$(TEST) --last-failed

lint: ## Lint code
	$(VENV)/bin/pylint $(CODE)
	$(VENV)/bin/mypy $(CODE)
	$(VENV)/bin/pydocstyle $(CODE)

format: ## Format all files
	isort $(CODE)
	black $(CODE)

install: ## Install all dependencies
	poetry install --no-interaction
